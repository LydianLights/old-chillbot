﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using System.Text.RegularExpressions;

namespace Chillbot.Commands.DiceCommands
{
    class Roll : ICommand
    {
        public string CommandName { get; } = "roll";
        public string[] Aliases { get; } = { String.Empty };
        public string Description { get; } = "Rolls a dice. Supports standard d20 notation.";
        public string Parameters { get; } = "<diceExpression>";

        //the bot this command is registered to
        private Bot Bot;

        //registers the command to the given bot
        public void RegisterTo(Bot bot)
        {
            Bot = bot;

            Bot.Commands.CreateCommand(CommandName)
                .Alias(Aliases)
                .Description(Description)
                .Parameter("_diceExpression", ParameterType.Multiple)
                .Do(e => { Execute(e); });
        }

        public async void Execute(CommandEventArgs e)
        {
            string[] args = e.Args;
            string diceExpression;
            int[][] results;
            int total = 0;
            bool invalidExpression = false;

            if (args.Length == 0)
            {
                //default to d100
                diceExpression = "d100";
            }
            else
            {
                //concatenate each argument into one expression
                //3d6+d4-5
                diceExpression = args[0];
                for (int i = 1; i < args.Length; i++)
                {
                    diceExpression += args[i];
                }
            }
            //divide into expressions separated by +/-
            //3d6, +d4, -5
            string[] diceGroups = Regex.Split(diceExpression, @"(?=[+-])");

            //remove empty entry from leading +/-
            if (diceGroups[0] == String.Empty)
            {
                diceGroups = diceGroups.Skip(1).ToArray();
            }

            //marks if the corresponding dice group should be added or subtracted
            bool[] isNegative = new bool[diceGroups.Length];
            //set up number of dice result groups we will need to store
            results = new int[diceGroups.Length][];

            for (int i = 0; i < diceGroups.Length && invalidExpression == false; i++)
            {
                //remove leading +/-
                //3d6, d4, 5
                string currentExpression = diceGroups[i];
                if (currentExpression[0] == '+')
                {
                    currentExpression = currentExpression.Substring(1);
                }
                else if (currentExpression[0] == '-')
                {
                    isNegative[i] = true;
                    currentExpression = currentExpression.Substring(1);
                }

                //if expression is empty, dice expression had invalid +/- in it
                if (currentExpression == string.Empty)
                {
                    invalidExpression = true;
                    break;
                }

                //split each dice group on the 'd'
                //3, d6 | d4 | 6
                string[] tokens = Regex.Split(currentExpression, @"(?=[d])");
                //leading 'd' makes an empty entry in the string[]
                if (tokens[0] == String.Empty)
                {
                    tokens = tokens.Skip(1).ToArray();
                }

                //initialize roll parameters
                int numberOfDice = 1;
                int diceType = 1;

                //if expression is only one part
                if (tokens.Length == 1)
                {
                    //if die (e.g. d4)
                    if (tokens[0][0] == 'd')
                    {
                        string n = tokens[0].Substring(1);
                        //if tryparse fails, n is not a number
                        //else n is stored to die
                        invalidExpression = !Int32.TryParse(n, out diceType);
                    }
                    //if number (e.g. 6)
                    else
                    {
                        string n = tokens[0];
                        //if tryparse fails, n is not a number
                        //else n is stored to multiplier
                        invalidExpression = !Int32.TryParse(n, out numberOfDice);
                    }
                }
                //if expression is two parts
                else if (tokens.Length == 2)
                {
                    string n = tokens[0];
                    //if tryparse fails, n is not a number
                    //else n is stored to multiplier
                    invalidExpression = !Int32.TryParse(n, out numberOfDice);

                    n = tokens[1].Substring(1);
                    //if tryparse fails, n is not a number
                    //else n is stored to die
                    invalidExpression = !Int32.TryParse(n, out diceType);
                }
                else
                {
                    invalidExpression = true;
                }

                //if values are valid, do the rolls
                if (invalidExpression == true)
                {
                    break;
                }
                else
                {
                    //random number
                    int r;
                    //results for this dice group
                    int[] groupResults;

                    //group is plain integer
                    if (diceType == 1)
                    {
                        groupResults = new int[1];
                        groupResults[0] = numberOfDice;

                        //add or subtract result to total
                        if (isNegative[i] == true)
                        {
                            total -= numberOfDice;
                        }
                        else
                        {
                            total += numberOfDice;
                        }
                    }
                    //group is dice
                    else
                    {
                        groupResults = new int[numberOfDice];

                        //roll the dice
                        for (int x = 0; x < numberOfDice; x++)
                        {
                            r = Bot.Rng.Next(0, diceType) + 1;
                            groupResults[x] = r;

                            //add or subtract result to total
                            if (isNegative[i] == true)
                            {
                                total -= r;
                            }
                            else
                            {
                                total += r;
                            }
                        }
                    }
                    //store the results
                    results[i] = groupResults;
                }
            }
            //failed roll
            if (invalidExpression == true)
            {
                await e.Channel.SendMessage("FUUUUUU");
            }
            //write results to chat
            else
            {
                string resultMessage = $"{e.User.Name} rolls the dice! Result:\n";
                //iterate through results to build result message
                for (int i = 0; i < results.Length; i++)
                {
                    //add proper +/- between roll groups
                    if (isNegative[i] == true)
                    {
                        if (i > 0)
                        {
                            resultMessage += " - ";
                        }
                        else
                        {
                            resultMessage += "-";
                        }
                    }
                    else if (i > 0)
                    {
                        resultMessage += " + ";
                    }

                    //begin reading roll group
                    resultMessage += "[";
                    for (int j = 0; j < results[i].Length; j++)
                    {
                        resultMessage += results[i][j].ToString();
                        if (j < results[i].Length - 1)
                        {
                            resultMessage += ", ";
                        }
                    }
                    resultMessage += "]";
                }
                resultMessage += $"\nTotal: {total}";

                await e.Channel.SendMessage(resultMessage);
            }
        }
    }
}
