﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;

namespace Chillbot.Commands
{
    interface ICommand
    {
        //the name used to invoke the command
        string CommandName { get; }
        //any aliases that may be used to invoke the command
        string[] Aliases { get; }
        //the description given to the user upon invoking the 'help' command
        string Description { get; }
        //the parameters of the command, displayed when invoking the 'help' command
        string Parameters { get; }

        //registers the command to the given bot
        void RegisterTo(Bot Bot);
        //invokes the command
        void Execute(CommandEventArgs e);
    }
}
