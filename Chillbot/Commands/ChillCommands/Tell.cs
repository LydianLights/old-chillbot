﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using System.Text.RegularExpressions;

namespace Chillbot.Commands.ChillCommands
{
    class Tell : ICommand
    {
        public string CommandName { get; } = "tell";
        public string[] Aliases { get; } = { String.Empty };
        public string Description { get; } = "Chillbot says something to the named person.";
        public string Parameters { get; } = "<User> <Message>";

        //the bot this command is registered to
        private Bot Bot;

        //registers the command to the given bot
        public void RegisterTo(Bot bot)
        {
            Bot = bot;

            Bot.Commands.CreateCommand(CommandName)
                .Alias(Aliases)
                .Description(Description)
                .Parameter("", ParameterType.Unparsed)
                .Do(e => { Execute(e); });
        }

        public async void Execute(CommandEventArgs e)
        {
            string[] args = Regex.Split(e.Args[0], @"^(<@\d+>)\s+");
            string message = String.Empty;
            string targetUser = String.Empty;
            bool invalidExpression = false;

            //check that regex matched
            if (args.Length == 3)
            {
                //args[0] = empty
                //args[1] = targetUser
                //args[2] = remaining message
                targetUser = args[1];
                message = e.Args[0];

                if (DiscordHelperClass.IsStringMentionID(targetUser) == false)
                {
                    invalidExpression = true;
                }
            }
            else
            {
                invalidExpression = true;
            }

            //if expression was invalid
            if (invalidExpression == true)
            {
                await e.Channel.SendMessage("FUUUUUU");
            }
            //send the designated message
            else
            {
                if (targetUser != $"<@{Bot.ID}>")
                {
                    if (Bot.EnableCommandDeletion == true)
                    {
                        await e.Message.Delete();
                    }
                    await e.Channel.SendMessage(message);
                }
                else
                {
                    await e.Channel.SendMessage(":smile_cat:");
                }
            }
        }
    }
}
