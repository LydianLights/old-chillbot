﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chillbot
{
    static class DiscordHelperClass
    {
        //returns true if the string is a user ID of the form <@###################>
        public static bool IsStringMentionID(string s)
        {
            if (s.Length > 3)
            {
                char firstChar = s[0];
                char secondChar = s[1];
                char lastChar = s[s.Length - 1];
                string numID = s.Substring(2, s.Length - 3);

                if (firstChar == '<' && secondChar == '@' && lastChar == '>')
                {
                    if (numID.All(char.IsDigit))
                    {
                        return true;
                    }
                }
            }

            return false;
        }
    }
}
